const crypto = require('crypto');

function getIssuedAt() {
    return Buffer.from(`iat: ${Math.floor(Date.now() / 1000) * 60}\n`).toString('base64') + ".";
}

function getDigest(key, iat) {
    return crypto.createHmac('sha1', key).update(iat).digest('base64');
}

function createToken(authKey) {
    const [mid,key] = authKey.split(":");
    const decodedKey = Buffer.from(key, 'base64');
    const iat = getIssuedAt();
    const digest = getDigest(decodedKey, iat);
    return `${mid}:${iat}.${digest}`;
}
module.exports = { createToken };
//console.log(createToken("u060c7749622e45647b472724da258666:RECUGN5a6tmO3wlt988Q"));
