"use client";
import { useState } from "react";
import { createToken } from "./test"; // Importing createToken function from test.js

export default function Card() {
  const [inputValue, setInputValue] = useState(""); // State to store input value
  const [token, setToken] = useState(""); // State to store generated token
  const [copySuccess, setCopySuccess] = useState(""); // State to store copy success message

  // Function to handle input change
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  // Function to generate token
  const generateToken = () => {
    const authKey = inputValue; // Assuming inputValue is your authentication key
    const generatedToken = createToken(authKey);
    setToken(generatedToken);
    setCopySuccess(""); // Reset copy success message
  };

  // Function to copy token to clipboard
  const copyToClipboard = () => {
    navigator.clipboard.writeText(token)
      .then(() => {
        setCopySuccess("Token copied to clipboard!");
      })
      .catch(() => {
        setCopySuccess("Failed to copy token.");
      });
  };

  return (
    <div>
      <input type="text" value={inputValue} onChange={handleInputChange} />
      <button onClick={generateToken}>Generate Token</button>
      <div>
        <h2>Token:</h2>
        <p>{token}</p>
        {token && (
          <button onClick={copyToClipboard}>Copy to Clipboard</button>
        )}
        {copySuccess && <p>{copySuccess}</p>}
      </div>
    </div>
  );
}
