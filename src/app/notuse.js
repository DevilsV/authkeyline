"use client"; // This is a client component 👈🏽
import { useState } from "react";
import { createToken } from "./test"; // Importing createToken function from test.js

export default function Card() {
  const [inputValue, setInputValue] = useState(""); // State to store input value
  const [token, setToken] = useState(""); // State to store generated token

  // Function to handle input change
  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  // Function to generate token
  const generateToken = () => {
    const authKey = inputValue; // Assuming inputValue is your authentication key
    const generatedToken = createToken(authKey);
    setToken(generatedToken);
  };

  return (
    <div>
      <input type="text" value={inputValue} onChange={handleInputChange} />
      <button onClick={generateToken}>Generate Token</button>
      <div>
        <h2>Token:</h2>
        <p>{token}</p>
      </div>
    </div>
  );
}
