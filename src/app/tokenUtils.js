// tokenUtils.js

export function generateTokenFromText(text) {
  // Logic to generate a token from the text
  const token = text.trim().toUpperCase().replace(/\s+/g, '-');
  return token;
}
